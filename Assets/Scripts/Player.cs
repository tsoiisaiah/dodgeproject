﻿using UnityEngine;
using System.Collections;

/**
 *	Player class is attached to the player prefab 
 *  this class the player to move with both kb and finger swipe(with mobile device)
 * 
 **/

public class Player : MonoBehaviour {

	// the tile that the player on
	Tile t;
	int hp = 1;

	// to prevent player keep moving in one direction
	Tile.Direction lastDirection = Tile.Direction.stationary;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Tile.Direction swipe = Tile.Direction.stationary;

		if (Input.touchCount > 0) {
			Touch touch = Input.GetTouch(0);

			if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Moved) {

				bool horizon = false;

				// times 1000 to keep the small movement
				// too small will become 0, and coz bug movement
				float deltaX = touch.deltaPosition.x * 1000;
				float deltaY = touch.deltaPosition.y * 1000;
				float absDeltaX = Mathf.Abs(deltaX);
				float absDeltaY = Mathf.Abs(deltaY);

				// only assign swipe if there is finger movement
				if (absDeltaX > 0f || absDeltaY > 0f) {
					if (absDeltaX > absDeltaY) horizon = true;

					float biggerDelta = (absDeltaX > absDeltaY) ? deltaX : deltaY;
					float direction = Mathf.Sign (biggerDelta);

					// as we have times 1000, we need to prevent relatively small movement
					// sensitive need to adjust base on user experience
					if (Mathf.Abs(biggerDelta) > 500) {
						// assign the direction
						if (!horizon && biggerDelta > 1) swipe = Tile.Direction.up;
						else if (!horizon && biggerDelta < 1) swipe = Tile.Direction.down;
						else if (horizon && biggerDelta < 1) swipe = Tile.Direction.left;
						else if (horizon && biggerDelta > 1) swipe = Tile.Direction.right;
						else swipe = Tile.Direction.stationary;

						Debug.Log(swipe);

						// move, only when it is different from last direction
						if (!swipe.Equals(lastDirection)) {
							move (swipe);
							lastDirection = swipe;
						}
					}
				}

				// reset lastDirection after finger lift
				if (touch.phase == TouchPhase.Ended) {
					lastDirection = Tile.Direction.stationary;
				}
			}
			// finger stay on screen, not liftinng
			// so, we do not reset lastDirection
			else if (touch.phase == TouchPhase.Stationary) {
//				Debug.Log ("Stationary");
				swipe = Tile.Direction.stationary;
			}
		}


		if (Input.GetKeyDown ("up")) {
			move (Tile.Direction.up);
		}
		else if (Input.GetKeyDown ("down")) {
			move (Tile.Direction.down);
		}
		else if (Input.GetKeyDown ("left")) {
			move (Tile.Direction.left);
		}
		else if (Input.GetKeyDown ("right")) {
			move (Tile.Direction.right);
		}
	}

	// move play to the target direction
	// checking if there is a tile and is reachable
	void move(Tile.Direction dir) {
		Tile target = t.getNeighbor (dir);

		Debug.Log ("target is null?(" + dir + ")" + (target == null));

		// adjust the tile
		// move to target if it is avaliable
		if ( target != null && target.isReachable() ) {
			t = target;
		}
		else {
			Debug.Log("aiya");
		}

		// perform the move for player
//		gameObject.transform.position = new Vector3 (t.position.x, t.position.y, gameObject.transform.position.z);
		moveToTile ();
	}
	void moveToTile () {
		gameObject.transform.position = new Vector3 (t.position.x, t.position.y, gameObject.transform.position.z);
	}
	
	public Vector3 getTilePosition () {
		return t.position;
	}
	public Tile getTile () {
		return t;
	}
	public void setTile (Tile tt) {
		t = tt;
		moveToTile ();
	}
	
	public bool isAlive () {
		return (hp > 0);
	}

	public void isHit () {
		if (hp <= 0) return ;

		hp -= 1;

		if (hp <= 0) {
			// object die
			gameObject.SetActive(false);
			TimeManager.setEndTime();

			Invoke ("dead", 1f);
//			toMenuScene();
		}
	}

	
	void OnTriggerStay(Collider other) {
//		Debug.Log ("bibibibibibi: " + other.name);
		isHit ();

	}

	void dead () {
		Destroy (gameObject);
		toMenuScene ();
	}

	void toMenuScene() {
		Application.LoadLevel ("menu");
	}
}
