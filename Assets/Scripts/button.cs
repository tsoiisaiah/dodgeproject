﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// TODO
// ugly code, redo later

public class button : MonoBehaviour {

	string thisColor;

	// Use this for initialization
	void Start () {
		thisColor = name.Replace ("_button", "");
	}

	void OnMouseDown () {
		Debug.Log (thisColor + " is pressed");

		// get the first cube from list
		GameObject cube = GlobalController.cubeQueue.Peek ();
		string getColor = cube.name.Replace ("(Clone)", "");

		// correct
		if (thisColor == getColor) {
			GlobalController.setMsg("great!");

			Debug.Log("match: " + thisColor);
			cube = GlobalController.cubeQueue.Dequeue();

			// destroy the object
			Destroy(cube);
		}
		// wrong
		else {
			GlobalController.setMsg("opps...wrong");
			Invoke ("backToMenu", 1);
			disableButton ();
		}

	}

	void disableButton() {
		GameObject[] buttons;
		buttons = GameObject.FindGameObjectsWithTag("button");
		foreach (GameObject button in buttons) {
			button.SetActive(false);
		}
	}

	void backToMenu() {
		Application.LoadLevel ("menu");
	}
}
