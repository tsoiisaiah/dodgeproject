﻿using UnityEngine;
using System.Collections;

public class ObjectManager : MonoBehaviour {

	public GameObject playerPrefab;
	public GameObject monsterPrefab;

	Tile[,] tileArray;
	int noOfRow;
	int nowOfColumn;

	Player playerScript;

	// Use this for initialization
	void Start () {
		// define the tielArray info first
		tileArray = TileManager.tileArray;
		
		noOfRow = tileArray.GetLength (0);
		nowOfColumn = tileArray.GetLength (1);

		// assign player in random place
		int x = Random.Range (0, noOfRow);
		int y = Random.Range (0, nowOfColumn);

		Tile t = tileArray [x, y];

		Debug.Log ("tile selected is [" + x + "][" + y + "]");
		Debug.Log ("tile position (" + t.position.x + ", " + t.position.y + ")");
		
		GameObject player = (GameObject)Instantiate (playerPrefab);
//		player.transform.position = new Vector3 (t.position.x, t.position.y, player.transform.position.z);
		
		playerScript = player.GetComponent<Player>();
		playerScript.setTile (t);

		// invoke monster generator
		InvokeRepeating ("generateMonsters", 2f, 1f);
	}
	
	// Update is called once per frame
	void Update () {

	}

	void generateMonsters () {
		int r = Random.Range (1, 3);
		for (int i=0; i < r; i++) {
			monsterGenerator ();
		}
	}

	void monsterGenerator () {
		// make a randome position
		int x = Random.Range (0, noOfRow);
		int y = Random.Range (0, nowOfColumn);
		
		Tile t = tileArray [x, y];
		// fix later
//		while ( (t.position == playerScript.getTilePosition()) ||
//		        (!t.isReachable())
//		       ){
//			t = tileArray [x, y];
//		}

		// tile indicate it is not reachable
		t.setReachable(false);

		// create the monster
		GameObject monster = (GameObject)Instantiate (monsterPrefab);

		Monster monsterScript = monster.GetComponent<Monster>();
		monsterScript.setTile (t);
	}
}
