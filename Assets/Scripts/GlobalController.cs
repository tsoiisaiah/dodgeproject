﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalController : MonoBehaviour {

	public static Queue<GameObject> cubeQueue;

	public GameObject markText;
	public static GameObject _markText;

	public GameObject msgText;
	public static GameObject _msgText;
	
	void Awake() {
		Debug.Log ("awake");
		// reset list everytime awake
		cubeQueue = new Queue<GameObject>();

		// assign static object
		_msgText = msgText;
		_markText = markText;

		setMsg("Let's Start");
		updateMark ();
	}
	
	public static void addMark() {
		Debug.Log ("add");
		int currMark = PlayerPrefs.GetInt ("mark", -1) + 1;
		PlayerPrefs.SetInt ("mark", currMark);

		updateMark ();
	}
	public static void updateMark() {
		TextMesh t = (TextMesh)_markText.GetComponent(typeof(TextMesh));
		t.text = "Mark: " + PlayerPrefs.GetInt ("mark", 0);
	}

	public static void setMsg(string msg) {
		Debug.Log ("set");
		GUIText t = (GUIText)_msgText.GetComponent(typeof(GUIText));
		t.text = msg;
	}






	// debug purpose printing
	void Start () {
		Debug.Log ("Start");
//		Invoke("showList", 2);
	}

	void showList() {
		int count = 0;
		foreach (GameObject cube in cubeQueue) {
			Debug.Log(count++ + ": " + cube.transform.name + ";" + cube.transform.position);
		}
	}
}
