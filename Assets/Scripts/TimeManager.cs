﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour {

	static float startTime;
	static float endTime;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void setEndTime () {
		endTime = Time.time;
		PlayerPrefs.SetFloat ("mark", endTime - startTime);
	}
}
