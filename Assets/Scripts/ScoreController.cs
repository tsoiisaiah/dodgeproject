﻿using UnityEngine;
using System.Collections;

public class ScoreController : MonoBehaviour {

	public GameObject highestScore;
	public GameObject lastScore;

	// Use this for initialization
	void Start () {
		float highest = PlayerPrefs.GetFloat ("highest");
		float last = PlayerPrefs.GetFloat ("mark");
		// get highest score
		if (last > highest) highest = last;
		PlayerPrefs.SetFloat ("highest", highest);

		TextMesh t = (TextMesh)highestScore.GetComponent(typeof(TextMesh));
		t.text = "Highest Score: " + highest;

		t = (TextMesh)lastScore.GetComponent(typeof(TextMesh));
		t.text = "Last Score: " + last;

		// reset
		PlayerPrefs.SetFloat ("mark", 0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
