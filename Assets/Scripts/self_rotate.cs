﻿using UnityEngine;
using System.Collections;

public class self_rotate : MonoBehaviour {
	public float x;
	public float y;
	public float z;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(x,y,z);

		float cx = gameObject.transform.rotation.x;
		float cy = gameObject.transform.rotation.y;
		float cz = gameObject.transform.rotation.z;

		if (cx < 0) cx += 360;
     	if (cy < 0) cy += 360;
      	if (cz < 0) cz += 360;

		Quaternion q = new Quaternion (cx, cy, cz, gameObject.transform.rotation.z);
		if (!q.Equals(gameObject.transform.rotation)) {
			gameObject.transform.rotation.Set(cx, cy, cz, gameObject.transform.rotation.z);
		}

	}
}
