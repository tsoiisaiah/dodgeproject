﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalVariables : MonoBehaviour {

	public GameObject mark_3DText;
	public static GameObject mark3DText;

	public List<Color> color_list;

	public static List<Color> colorList;
	public static Color currColor;
	static int mark;

	// Use this for initialization
	void Start () {
		colorList = fixColorAlpha(color_list);
		mark3DText = mark_3DText;

		currColor = colorList [0];
		mark = 0;

		updateMark ();
	}

	public static void addMark () {
		mark++;
		updateMark ();
	}
	public static void minusMark () {
		mark--;
		updateMark ();
	}
	static void updateMark () {
		TextMesh markText = (TextMesh)mark3DText.GetComponent(typeof(TextMesh));
		markText.text = "Mark: " + mark;
	}

	// Update is called once per frame
	void Update () {
	
	}

	List<Color> fixColorAlpha (List<Color> oriList) {
		List<Color> newList = new List<Color>();

		foreach (Color c in oriList) {
			newList.Add(new Color(c.r, c.g, c.b, 1f));
		}

		return newList;
	}
	
}
