﻿using UnityEngine;
using System.Collections;

// for every monster

public class Monster : MonoBehaviour {

	Tile t;
	public float actionDelaySecond;
	public float actionLastSecond;

	GameObject laser;


	void Awake () {

	}

	// Use this for initialization
	void Start () {
		// count down n seconds and active the skill
		// invoke skill function after n seconds
		Invoke ("action", actionDelaySecond);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// skill
	void action () {
		laser = gameObject.transform.Find ("laser").gameObject;

		laser.SetActive (true);

		// turn off after n seconds
		Invoke ("stopAction", actionLastSecond);
	}

	void stopAction () {
		laser.SetActive (false);
//		Invoke ("selfDestroy", 0.25f);
		selfDestroy ();
	}

	// later may add some animation
	void selfDestroy () {
		t.setReachable (true);
		Destroy(gameObject);
	}

	// when monster spawn
	// show itself
	// tell tileMgr which block/row/column it will affect
	// if the effect is a ground effect
	// pass a callback function for the effect when calling tileMgr

	// callback function for special ground effect
	void specialGroundEffect () {

	}

	public Vector3 getTilePosition () {
		return t.position;
	}
	public Tile getTile () {
		return t;
	}
	public void setTile (Tile tt) {
		t = tt;
		moveToTile ();
	}
	void moveToTile () {
		gameObject.transform.position = new Vector3 (t.position.x, t.position.y, gameObject.transform.position.z);
	}
}
