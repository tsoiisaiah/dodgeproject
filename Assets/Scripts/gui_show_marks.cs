﻿using UnityEngine;
using System.Collections;

public class gui_show_marks : MonoBehaviour {

	public GUIText guiMark;

	// Use this for initialization
	void Start () {
		Debug.Log("Marks: " + PlayerPrefs.GetInt("mark"));
		guiMark.text = "Marks: " + PlayerPrefs.GetInt ("mark");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
