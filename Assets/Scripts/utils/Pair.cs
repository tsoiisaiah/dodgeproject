﻿using UnityEngine;
using System.Collections;

public class Pair<T1, T2> {

	T1 first;
	T2 second;

	public Pair (T1 x, T2 y) {
		first = x;
		second = y;
	}

	public void setFirst (T1 x) {
		first = x;
	}
	public void setSecond (T2 x) {
		second = x;
	}

	public T1 getFirst () {
		return first;
	}
	public T2 getSecond () {
		return second;
	}

	public override string ToString () {
		return "Pair(" + first + ", " + second + ")";
	}
}
