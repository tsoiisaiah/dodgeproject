﻿using UnityEngine;
using System.Collections;

// for every tile

public class Tile : ScriptableObject {
	
	public enum Direction {up, down, left, right, stationary};

	// neighbor
	public Tile up, down, left, right;
	// actual object
	GameObject o;
	// reachable by player?
	bool reachable = true;
	public Vector3 position;

	// Use this for initialization
	void Start () {
		// link with the actual block

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// every tile is monitor and create by tileMgr
	// tileMgr is told by monster on how the board(tiles) act like


	// count down effect
	// may pass a callback function from the monster
	// for any special ground effect
	void countDown () {

	}

	public void setObject (GameObject b) {
		o = b;
		position = b.transform.position;
	}

	public bool isReachable () {
		return reachable;
	}
	public void setReachable (bool reachable) {
		this.reachable = reachable;
	}

	public Tile getNeighbor (Direction n) {
		Tile nt = null;

		Debug.Log ("getNeighbor: " + up + "," + down + "," + left + "," + right);
		
		switch (n) {
		case Tile.Direction.up:
			nt = up;
			break;
		case Tile.Direction.down:
			nt = down;
			break;
		case Tile.Direction.left:
			nt = left;
			break;
		case Tile.Direction.right:
			nt = right;
			break;
		}

		return nt;
	}

	public override string ToString () {
		return "Tile(" + position.x + ", " + position.y + ")";
	}

}
