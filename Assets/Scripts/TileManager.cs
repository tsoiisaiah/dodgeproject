﻿using UnityEngine;
using System.Collections;

public class TileManager : MonoBehaviour {

	public static Tile[,] tileArray;
	public GameObject blockPrefab;
	public Vector3 topLeft;
	public Vector3 bottomRight;

	// phase 1 do not calculate for good
	// later will need to adjust the size of the block according to the no of row and column
//	public int noOfRow;
//	public int noOfColumn;

	int noOfRow, noOfColumn;	
	int ax = 0;
	int ay = 0;


	// Use this for initialization
	void Start () {
		// create the playground
		// no error checking for phase 1
		float beginX = topLeft.x;
		float beginY = topLeft.y;
		float endX = bottomRight.x;
		float endY = bottomRight.y;

		// size of the array
		noOfRow = (int)(Mathf.Abs (beginX) + Mathf.Abs (endX) + 1);
		noOfColumn = (int) (Mathf.Abs (beginY) + Mathf.Abs (endY) + 1);
		tileArray = new Tile [noOfRow, noOfColumn];
//		Debug.Log ("size [" + noOfRow + "," + noOfColumn + "]");

		// xy in position
		for (float y = beginY; y >= endY; y--) {
			for (float x = beginX; x <= endX; x++) {
				createBlock(x, y);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void createBlock (float x, float y) {
		GameObject b = (GameObject)Instantiate (blockPrefab);
		b.transform.position = new Vector3 (x, y, 0);

		// assign the tile
		Tile t = ScriptableObject.CreateInstance<Tile> ();
		t.setObject (b);

		// save to array
//		Debug.Log ("saving to [" + ax + "," + ay + "]");
		tileArray [ax, ay] = t;
		
		// set the neighbor
		// only need to check up and left
		matchWithNeighbor (t, Tile.Direction.up);
		matchWithNeighbor (t, Tile.Direction.left);
		
		// calculate next cell in the array
		if (++ax >= noOfRow) {
			ax = 0;
			ay++;
		}
	}

	void matchWithNeighbor(Tile t, Tile.Direction n) {
		Pair<int, int> anp = getNeighborPosition (n);

		if (anp == null) return;

		Tile nt = null;
		try {
			nt = tileArray[anp.getFirst(), anp.getSecond()];
		} catch (System.IndexOutOfRangeException e) {}

		if (nt == null) return;

		if (n == Tile.Direction.up) {
			t.up = nt;
			nt.down = t;
//			Debug.Log("found up (" + anp.getFirst() + ", " + anp.getSecond() + ")");
		}
		else if (n == Tile.Direction.left) {
			t.left = nt;
			nt.right = t;
//			Debug.Log("found left (" + anp.getFirst() + ", " + anp.getSecond() + ")");
		}
	}

	Pair<int, int> getNeighborPosition(Tile.Direction n) {
		Pair<int, int> nv = null;
		switch (n) {
		case Tile.Direction.up:
			nv = new Pair<int, int> (ax, ay-1);
			break;
		case Tile.Direction.left:
			nv = new Pair<int, int> (ax-1, ay);
			break;
		}

		return nv;
	}



}
